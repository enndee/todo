import React from "react";
import ListItem from "./listItem";

const List = ({ items, onChange, onDelete }) => {
  return (
    <div className="list-container">
      <div className="list-background"></div>
      <div className="list">
        {items.map(item => (
          <ListItem item={item} onChange={onChange} onDelete={onDelete} key={item.id} />
        ))}
      </div>
    </div>
  );
};

export default List;
