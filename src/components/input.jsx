import React from "react";

const Input = ({ labelText, submitButtonText, inputId, onChange, onSubmit, value }) => {
  return (
    <form onSubmit={onSubmit}>
      <label htmlFor={inputId}>{labelText}</label>
      <input type="text" id={inputId} value={value} onChange={e => onChange(e.currentTarget.value)} />
      <input type="submit" value={submitButtonText}></input>
    </form>
  );
};

export default Input;
