import React, { Component } from "react";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class ListItem extends Component {
  state = { hovering: false };

  handleHover = () => {
    this.setState({ hovering: true });
  };

  handleHoverOff = () => {
    this.setState({ hovering: false });
  };

  render() {
    const { item, onChange, onDelete } = this.props;

    return (
      <div
        className="list-item"
        onMouseEnter={this.handleHover}
        onMouseLeave={this.handleHoverOff}
        onFocus={this.handleHover}
        onBlur={this.handleHoverOff}
      >
        <div>
          <span
            className={`${item.done ? "list-item-text strikethrough" : "list-item-text"}`}
            onClick={() => onChange(item)}
          >
            {item.text}
          </span>
          {item.done && (
            <span role="alert" className="screen-reader-text">
              {item.text} is complete
            </span>
          )}
        </div>

        <button
          onClick={() => onDelete(item)}
          className={`delete-button ${this.state.hovering ? "show-delete" : "hide-delete"}`}
        >
          <FontAwesomeIcon icon={faTrashAlt} size="lg" />
          <span className="screen-reader-text">Delete {item.text}</span>
        </button>
      </div>
    );
  }
}

export default ListItem;
