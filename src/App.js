import React, { Component } from "react";
import Input from "./components/input";
import List from "./components/list";
import "./index.css";

class App extends Component {
  state = {
    list: [],
    inputValue: ""
  };

  componentDidMount() {
    const list = JSON.parse(localStorage.getItem("list")) || [];
    this.setState({ list });
  }

  handleInputChange = inputString => {
    this.setState({ inputValue: inputString });
  };

  handleInputSubmit = e => {
    e.preventDefault();

    if (this.state.inputValue.length) {
      const list = [...this.state.list, { text: this.state.inputValue, done: false, id: Date.now() }];
      this.setState({ list, inputValue: "" }, () => {
        this.saveList();
      });
    }
  };

  handleDelete = item => {
    const list = this.state.list.filter(i => i !== item);
    this.setState({ list }, () => {
      this.saveList();
    });
  };

  handleTaskDone = item => {
    const list = [...this.state.list];
    const index = list.indexOf(item);

    list[index] = { ...list[index] };
    list[index].done = !list[index].done;
    this.setState({ list }, () => {
      this.saveList();
    });
  };

  handleClearAll = () => {
    console.log("clearing");
    this.setState({ list: [] }, () => {
      this.saveList();
    });
  };

  saveList = () => {
    localStorage.setItem("list", JSON.stringify(this.state.list));
  };

  render() {
    const { inputValue, list } = this.state;

    return (
      <div className="app container">
        <h1>ToDo</h1>
        <Input
          value={inputValue}
          labelText={"Add a new item:"}
          submitButtonText={"Add Item"}
          inputId={"new-item"}
          onChange={this.handleInputChange}
          onSubmit={this.handleInputSubmit}
        />
        <List items={list} onDelete={this.handleDelete} onChange={this.handleTaskDone} />
        <div className="centre">
          <button onClick={this.handleClearAll} className="clear-all-button">
            Clear All Items
          </button>
        </div>
      </div>
    );
  }
}

export default App;
